#include <random>
#include <benchmark/benchmark.h>

constexpr double pi() { return std::atan(1)*4; }
constexpr double sqrt_2_over_e = std::sqrt(2/std::exp(1));


template<typename F>
struct ratio_of_uniform2 {
	template<class RNG>
	F gen(RNG &gen) {
		std::uniform_real_distribution<F> u01;
		while(true) {
			F u = u01(gen);
			F v = (u01(gen) * 2 - 1) * sqrt_2_over_e;
			F x = v/u;
			if(x*x <= 6 - 8*u + 2*u*u) return x;
			else if(x*x >= 2/u - 2*u) continue;
			else if(x*x <= -4*std::log(u)) return x;
		}
	}
};

template<typename F>
struct normal_box_mueller {
	bool hot = false;
	F up;
	template<class RNG>
	F gen(RNG &gen) {
		if(hot) {
			hot = false;
			return up;
		} else {
			std::uniform_real_distribution<F> u01;
			F u1 = u01(gen);
			F u2 = u01(gen);
			F factor = std::sqrt(-2*std::log(u1));
			F z1 = factor*std::cos(2*pi()*u2);
			F z2 = factor*std::sin(2*pi()*u2);
			up = z2;
			hot = true;
			return z1;
		}
	}
};

template<typename F>
struct marsaglia {
	bool hot = false;
	F up;
	template<class RNG>
	F gen(RNG &gen) {
		if(hot) {
			hot = false;
			return up;
		} else {
			std::uniform_real_distribution<F> u01;
			F u1, u2, S;
			do {
				u1 = 2*u01(gen) - 1;
				u2 = 2*u01(gen) - 1;
				S = u1*u1 + u2*u2;	
			} while (S > 1.0 || S == 0);
			F factor = std::sqrt(-2*std::log(u01(gen)) / S);	
			up = u1 * factor;
			hot = true;
			return u2 * factor;
		}
	}
};

static void BM_Box_Mueller(benchmark::State& state) {
	std::mt19937_64 gen;
	normal_box_mueller<double> dist;
	for (auto _ : state) {
		benchmark::DoNotOptimize(dist.gen(gen));
	}
}

static void BM_Marsaglia(benchmark::State& state) {
	std::mt19937_64 gen;
	marsaglia<double> dist;
	for (auto _ : state) {
		benchmark::DoNotOptimize(dist.gen(gen));
	}
}


static void BM_Ratio_of_uniforms(benchmark::State& state) {
	std::mt19937_64 gen;
	ratio_of_uniform2<double> dist;
	for (auto _ : state) {
		benchmark::DoNotOptimize(dist.gen(gen));
	}
}

static void BM_Default(benchmark::State& state) {
	std::mt19937_64 gen;
	std::normal_distribution<double> dist;
	for (auto _ : state) {
		benchmark::DoNotOptimize(dist(gen));
	}
}

static void BM_MT(benchmark::State& state) {
	std::mt19937_64 gen;
	std::uniform_real_distribution<double> dist;
	for (auto _ : state) {
		benchmark::DoNotOptimize(dist(gen));
	}
}

// Register the function as a benchmark
BENCHMARK(BM_Box_Mueller);
BENCHMARK(BM_Marsaglia);
BENCHMARK(BM_Ratio_of_uniforms);
BENCHMARK(BM_Default);
BENCHMARK(BM_MT);
// Run the benchmark
BENCHMARK_MAIN();
